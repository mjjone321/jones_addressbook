package junk;

public class AddressEntry {
	private String firstName;    //create variable for first name
	private String lastName;     //create variable for last name
	private String street;       //create variable for street
	private String city;         //create variable for city
	private String state;        //create variable for state
	private Integer zip;         //create variable for zip
	private String phone;        //create variable for phone
	private String email;        //create variable for email
	
	//Default constructor
	public AddressEntry() 
	{
		firstName = null;
		lastName = null;
		street = null;
		city = null;
		state = null;
		zip = null;
		phone = null;
		email = null;
	}
	
	//constructor with all variable parameters
	public AddressEntry(String firstName,String lastName,String street,String city,
			String state, Integer zip,String phone,String email)
	{
		this.firstName = firstName;
		this.lastName = lastName;
		this.street = street;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.phone = phone;
		this.email = email;
	}
	
	//set first name
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	//return first name
	public String getFirstName()
	{
		return firstName;
	}
	
	//set last name
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	//return last name
	public String getLastName()
	{
		return lastName;
	}

	//set street
	public void setStreet(String street)
	{
		this.street = street;
	}
	//return street
	public String getStreet()
	{
		return street;
	}
	
	//set city
	public void setCity(String city)
	{
		this.city = city;
	}
	//return city
	public String getCity()
	{
		return city;
	}
	
	//set state
	public void setState(String state)
	{
		this.state = state;
	}
	//return state
	public String getState()
	{
		return state;
	}
	
	//set zip
	public void setZip(Integer zip)
	{
		this.zip = zip;
	}
	//return zip
	public Integer getZip()
	{
		return zip;
	}
	
	//set phone
	public void setPhone(String phone)
	{
		this.phone = phone;
	}
	//return phone
	public String getPhone()
	{
		return phone;
	}
	
	//set email
	public void setEmail(String email)
	{
		this.email = email;
	}
	//return email
	public String getEmail()
	{
		return email;
	}
	
	public String toString()
	{
		return "First Name: " +getFirstName() +"\nLast Name: " +getLastName()
			  +"\nStreet: " +getStreet() +"\nCity: " +getCity() +"\nState: " 
		      +getState() +"\nZip: " +getZip() +"\nPhone: " +getPhone() 
		      +"\nEmail: " +getEmail(); 
	}
}