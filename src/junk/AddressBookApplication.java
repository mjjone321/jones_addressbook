package junk;
import java.io.*;
public class AddressBookApplication 
{
	public static void main(String[] args) throws IOException
	{
		init("AddressInputDataFile.txt");
		AddressBook addressBook = new AddressBook();
		initAddressBookExercise(addressBook);
	}
	public static void initAddressBookExercise(AddressBook ab)
	{
		AddressEntry addressEntry0 = new AddressEntry();
		ab.add(addressEntry0);
		AddressEntry addressEntry1 = new AddressEntry();
		ab.add(addressEntry1);
		ab.list();
	}
	
	 static void init(String filename) throws IOException  
	{
		 BufferedReader file = new BufferedReader(new FileReader(filename));
		 String contact;
		 while ((contact = file.readLine()) != null)
		 {
			 System.out.println(contact);
		 }
		 file.close();
	}
}